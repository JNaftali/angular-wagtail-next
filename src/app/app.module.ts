import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { AngularWagtailModule } from 'angular-wagtail';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    AngularWagtailModule.forRoot({
      apiDomain: 'https://api-dev.devatech.us',
      siteId: 2,
      pageTypes: [
        {
          type: 'lazy',
          loadComponent: () =>
            import('./lazy/lazy.component').then((x) => x.LazyComponent),
        },
      ],
    }),
    TransferHttpCacheModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
