import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AngularWagtailComponent } from 'angular-wagtail';
import { GetPageDataResolverService } from 'angular-wagtail';

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: '**',
          component: AngularWagtailComponent,
          resolve: { poop: GetPageDataResolverService},
          runGuardsAndResolvers: 'always',
        },
      ],
      {
        initialNavigation: 'enabled',
      }
    ),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
