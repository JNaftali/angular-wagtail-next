import { Component } from '@angular/core';
import { AngularWagtailService } from 'angular-wagtail';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'angular-wagtail-demo';

  constructor(public wagtail: AngularWagtailService) {}
}
