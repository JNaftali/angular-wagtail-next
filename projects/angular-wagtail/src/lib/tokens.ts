import { InjectionToken } from '@angular/core';

export const WAGTAIL_CONFIG = new InjectionToken('Wagtail Config');
