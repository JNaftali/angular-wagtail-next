import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { AngularWagtailComponent } from './angular-wagtail.component';
import { AngularWagtailService } from './angular-wagtail.service';
import { IWagtailModuleConfig } from './interfaces';
import { GetPageDataResolverService } from './resolver.service';
import { WAGTAIL_CONFIG } from './tokens';

@NgModule({
  declarations: [AngularWagtailComponent],
  imports: [HttpClientModule],
  exports: [AngularWagtailComponent],
  providers: [AngularWagtailService],
})
export class AngularWagtailModule {
  static forRoot(
    config: IWagtailModuleConfig
  ): ModuleWithProviders<AngularWagtailModule> {
    return {
      ngModule: AngularWagtailModule,
      providers: [
        GetPageDataResolverService,
        { provide: WAGTAIL_CONFIG, useValue: config },
      ],
    };
  }
}
