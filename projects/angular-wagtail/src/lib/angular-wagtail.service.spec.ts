import { TestBed } from '@angular/core/testing';

import { AngularWagtailService } from './angular-wagtail.service';

describe('AngularWagtailService', () => {
  let service: AngularWagtailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AngularWagtailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
