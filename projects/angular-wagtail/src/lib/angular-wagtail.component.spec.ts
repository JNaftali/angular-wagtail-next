import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularWagtailComponent } from './angular-wagtail.component';

describe('AngularWagtailComponent', () => {
  let component: AngularWagtailComponent;
  let fixture: ComponentFixture<AngularWagtailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AngularWagtailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AngularWagtailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
