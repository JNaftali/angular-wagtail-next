import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';
import { AngularWagtailService } from './angular-wagtail.service';
import { exhaustMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class GetPageDataResolverService implements Resolve<unknown> {
  constructor(private wagtail: AngularWagtailService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // const draft = route.queryParamMap.get('draft');
    // return this.wagtail.getPageForUrl(state.url, draft);
    return this.wagtail
      .getPageForUrl(state.url)
      .pipe(exhaustMap((page) => this.wagtail.getComponentForPage(page)));
  }
}
