import {
  Component,
  ComponentFactoryResolver,
  Injector,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { AngularWagtailService } from './angular-wagtail.service';

@Component({
  selector: 'lib-angular-wagtail',
  template: ` <ng-container #wagtailOutlet></ng-container> `,
  styles: [],
})
export class AngularWagtailComponent {
  @ViewChild('wagtailOutlet', { read: ViewContainerRef })
  // @ts-ignore
  outlet: ViewContainerRef;

  constructor(
    private wagtail: AngularWagtailService,
    private cfr: ComponentFactoryResolver,
    private injector: Injector
  ) {}

  ngAfterViewInit() {
    this.wagtail.currentPageComponent.subscribe((component) => {
      if (component) {
        const factory = this.cfr.resolveComponentFactory(component);
        this.outlet.createComponent(factory, undefined, this.injector);
      }
    });
  }
}
