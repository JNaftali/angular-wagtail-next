import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { exhaustMap, mergeMap } from 'rxjs/operators';
import { IWagtailModuleConfig } from './interfaces';
import { WAGTAIL_CONFIG } from './tokens';

@Injectable({
  providedIn: 'root',
})
export class AngularWagtailService {
  private path = new BehaviorSubject(
    this.sanitizePathname(this.location.path())
  );

  currentPage = this.path.pipe(
    mergeMap((pathname) => {
      return this.getPageForUrl(pathname);
    })
  );

  currentPageComponent = this.currentPage.pipe(
    exhaustMap((page) => {
      return this.getComponentForPage(page);
    })
  );

  public getPageForUrl(url: string) {
    const pathname = this.sanitizePathname(url);
    const params = new URLSearchParams({ html_path: pathname });
    if (this.config.siteId) params.set('site', this.config.siteId.toString());
    return this.http.get<any>(
      `${this.config.apiDomain}${
        this.config.pagesApiPath ?? '/api/v2/pages/detail_by_path/'
      }?${params.toString()}`
    );
  }

  public async getComponentForPage(page: { type: string }) {
    const route = this.config.pageTypes.find(
      (route) => route.type === 'lazy' // TODO: use the page type from wagtail
    );

    if (!route) return; // TODO: return notfound

    if ('component' in route) {
      return route.component;
    } else {
      return await route.loadComponent();
    }
  }

  private sanitizePathname(path: string) {
    return path.split(/[?#]/)[0];
  }

  constructor(
    private http: HttpClient,
    private location: Location,
    @Inject(WAGTAIL_CONFIG) private config: IWagtailModuleConfig
  ) {
    location.onUrlChange((url) => this.path.next(this.sanitizePathname(url)));
  }
}
