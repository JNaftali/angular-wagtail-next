/*
 * Public API Surface of angular-wagtail
 */

export * from './lib/angular-wagtail.service';
export * from './lib/angular-wagtail.component';
export * from './lib/angular-wagtail.module';
export * from './lib/resolver.service';
